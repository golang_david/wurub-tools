package musimundo

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golang_david/wurub-tools/pkg/scraping"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	url2 "net/url"
	"strconv"
	"strings"
	"sync"
)

const (
	Name      = "Musimundo"
	domain    = "www.musimundo.com"
	urlSearch = "https://u.braindw.com/els/musimundoapi?ft=%s&qt=30&sc=emsa&refreshmetadata=true&exclusive=0&aggregations=trues"
)

type musimundo struct {
}

func NewSource() scraping.Source {
	return musimundo{}
}

func (m musimundo) Search(search string, chanResult chan<- scraping.Match, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[%s] Source %s recovered of panic: %s\n", Name, Name, r)
		}
	}()
	defer wg.Done()
	search = url2.QueryEscape(search)
	url := fmt.Sprintf(urlSearch, search)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}
	respMap := make(map[string]interface{}, 0)
	err = json.Unmarshal(body, &respMap)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	data := respMap["hits"].(map[string]interface{})
	products := data["hits"].([]interface{})
	for _, val := range products {
		prod := val.(map[string]interface{})["_source"].(map[string]interface{})
		title := prod["Descripcion"].(string)

		priceStr := prod["PrecioInternet"].(string)
		priceStr = strings.ReplaceAll(priceStr, " ", "")
		priceStr = strings.ReplaceAll(priceStr, "$", "")
		priceStr = strings.ReplaceAll(priceStr, ".", "")
		priceStr = strings.ReplaceAll(priceStr, ",", ".")
		price, err := strconv.ParseFloat(priceStr, 32)
		if err != nil {
			log.Printf("[%s] %s\n", Name, err)
		}

		link := prod["Link"].(string)

		chanResult <- scraping.Match{
			Source: Name,
			Title:  title,
			Price:  math.Round(price*100) / 100,
			Link:   link,
		}
	}
}
