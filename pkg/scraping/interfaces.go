package scraping

import "sync"

type Source interface {
	Search(search string, chanResult chan<- Match, wg *sync.WaitGroup)
}
