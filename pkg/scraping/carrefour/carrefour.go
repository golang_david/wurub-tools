package carrefour

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/golang_david/wurub-tools/pkg/scraping"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"sync"
)

const (
	Name       = "Carrefour"
	domain     = "www.carrefour.com.ar"
	urlSearch  = "https://www.carrefour.com.ar/_v/segment/graphql/v1?workspace=master&maxAge=short&appsEtag=remove&domain=store&locale=es-AR&__bindingId=ecd0c46c-3b2a-4fe1-aae0-6080b7240f9b&operationName=productSearchV3&variables={}&extensions=%s"
	extensions = "{\"persistedQuery\":{\"version\":1,\"sha256Hash\":\"4136d62c555a4b2e1ba9a484a16390d6a6035f51760d5726f436380fa290d0cc\",\"sender\":\"vtex.store-resources@0.x\",\"provider\":\"vtex.search-graphql@0.x\"},\"variables\":\"%s\"}"
	variables  = `{
   "hideUnavailableItems":false,
   "skusFilter":"ALL_AVAILABLE",
   "simulationBehavior":"default",
   "installmentCriteria":"MAX_WITHOUT_INTEREST",
   "productOriginVtex":false,
   "map":"ft",
   "query":"ps5",
   "orderBy":"",
   "from":0,
   "to":31,
   "selectedFacets":[
      {
         "key":"ft",
         "value":"ps5"
      }
   ],
   "fullText":"%s",
   "facetsBehavior":"Static",
   "categoryTreeBehavior":"default",
   "withFacets":false
}`
)

type carrefour struct {
}

func NewSource() scraping.Source {
	return carrefour{}
}

func (c carrefour) Search(search string, chanResult chan<- scraping.Match, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[%s] Source %s recovered of panic: %s\n", Name, Name, r)
		}
	}()
	defer wg.Done()
	variables := fmt.Sprintf(variables, search)
	bs64 := base64.StdEncoding.EncodeToString([]byte(variables))
	ext := fmt.Sprintf(extensions, bs64)
	url := fmt.Sprintf(urlSearch, ext)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}
	respMap := make(map[string]interface{}, 0)
	err = json.Unmarshal(body, &respMap)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	data := respMap["data"].(map[string]interface{})["productSearch"].(map[string]interface{})
	products := data["products"].([]interface{})
	for _, val := range products {
		prod := val.(map[string]interface{})

		title := prod["productName"].(string)

		item := prod["items"].([]interface{})[0].(map[string]interface{})
		seller := item["sellers"].([]interface{})[0].(map[string]interface{})
		commercialOffer := seller["commertialOffer"].(map[string]interface{})
		price := commercialOffer["Price"].(float64)

		quantity := commercialOffer["AvailableQuantity"].(float64)

		link := prod["linkText"].(string)
		link = fmt.Sprintf("https://%s/%s/p", domain, link)

		if quantity > 0 {
			chanResult <- scraping.Match{
				Source: Name,
				Title:  title,
				Price:  math.Round(price*100) / 100,
				Link:   link,
			}
		}
	}
}
