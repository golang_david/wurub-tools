package walmart

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golang_david/wurub-tools/pkg/scraping"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	url2 "net/url"
	"strconv"
	"strings"
	"sync"
)

const (
	Name      = "Walmart"
	domain    = "www.walmart.com.ar"
	urlSearch = "https://ucustom.walmart.com.ar/docs/search.json?bucket=walmart_search_stage&family=product&view=default&text=%s&window=30&sort=$_substance_value&direction=-1&levels=1&attributes[sales_channel][]=15&page=1"
)

type walmart struct {
}

func NewSource() scraping.Source {
	return walmart{}
}

func (w walmart) Search(search string, chanResult chan<- scraping.Match, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[%s] Source %s recovered of panic: %s\n", Name, Name, r)
		}
	}()
	defer wg.Done()
	search = url2.QueryEscape(search)
	url := fmt.Sprintf(urlSearch, search)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}
	respMap := make(map[string]interface{}, 0)
	err = json.Unmarshal(body, &respMap)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
		return
	}

	data := respMap["data"].(map[string]interface{})
	products := data["views"].([]interface{})
	for _, val := range products {
		prod := val.(map[string]interface{})
		title := prod["title"].(string)

		p := prod["sales_channel_data"].([]interface{})
		priceStr := p[0].(map[string]interface{})["best_price"].(string)
		priceStr = strings.ReplaceAll(priceStr, " ", "")
		priceStr = strings.ReplaceAll(priceStr, "$", "")
		priceStr = strings.ReplaceAll(priceStr, ".", "")
		priceStr = strings.ReplaceAll(priceStr, ",", ".")
		price, err := strconv.ParseFloat(priceStr, 32)
		if err != nil {
			log.Printf("[%s] %s\n", Name, err)
		}
		price = price / 100

		link := prod["permalink"].(string)
		link = fmt.Sprintf("https://%s/%s/p", domain, link)

		chanResult <- scraping.Match{
			Source: Name,
			Title:  title,
			Price:  math.Round(price*100) / 100,
			Link:   link,
		}
	}
}
