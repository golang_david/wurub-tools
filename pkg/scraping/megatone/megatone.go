package megatone

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	"gitlab.com/golang_david/wurub-tools/pkg/scraping"
	"log"
	"math"
	"strconv"
	"strings"
	"sync"
)

const (
	Name          = "Megatone"
	domain        = "www.megatone.net"
	urlSearch     = "https://www.megatone.net/search/%s/"
	querySelector = "div[class*='CajaProductoGrilla pr']"
)

type megatone struct {
}

func NewSource() scraping.Source {
	return megatone{}
}

func (m megatone) Search(search string, chanResult chan<- scraping.Match, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[%s] Source %s recovered of panic: %s\n", Name, Name, r)
		}
	}()
	defer wg.Done()
	c := colly.NewCollector(
		colly.AllowedDomains(domain),
	)
	extensions.RandomUserAgent(c)

	c.OnError(func(response *colly.Response, err error) {
		log.Printf("[%s] %s\n", Name, err)
	})
	c.OnResponse(func(response *colly.Response) {
		fmt.Printf("%s\n", response.Body)
	})

	callback := func(e *colly.HTMLElement) {
		title := e.ChildText("h3[class='itemBox--title']")

		priceStr := e.ChildText("span[class*='value-item ']")
		priceStr = strings.ReplaceAll(priceStr, " ", "")
		priceStr = strings.ReplaceAll(priceStr, "$", "")
		priceStr = strings.ReplaceAll(priceStr, ".", "")
		priceStr = strings.ReplaceAll(priceStr, ",", ".")
		price, err := strconv.ParseFloat(priceStr, 32)
		if err != nil {
			log.Printf("[%s] %s\n", Name, err)
		}

		link := e.ChildAttr("a[class='block']", "href")
		link = fmt.Sprintf("https://%s%s", domain, link)

		chanResult <- scraping.Match{
			Source: Name,
			Title:  title,
			Price:  math.Round(price*100) / 100,
			Link:   link,
		}
	}

	c.OnHTML(querySelector, callback)

	url := fmt.Sprintf(urlSearch, search)

	err := c.Visit(url)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
	}
}
