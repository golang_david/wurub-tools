package tiendabna

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	"gitlab.com/golang_david/wurub-tools/pkg/scraping"
	"log"
	"math"
	url2 "net/url"
	"strconv"
	"strings"
	"sync"
)

const (
	Name          = "TiendaBNA"
	domain        = "www.tiendabna.com.ar"
	urlSearch     = "https://www.tiendabna.com.ar/catalog?query=%s"
	querySelector = "article[class*='card']"
)

type tiendaBNA struct {
}

func NewSource() scraping.Source {
	return tiendaBNA{}
}

func (s tiendaBNA) Search(search string, chanResult chan<- scraping.Match, wg *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("[%s] Source %s recovered of panic: %s\n", Name, Name, r)
		}
	}()
	defer wg.Done()
	c := colly.NewCollector(
		colly.AllowedDomains(domain),
	)
	extensions.RandomUserAgent(c)

	c.OnError(func(response *colly.Response, err error) {
		log.Printf("[%s] %s\n", Name, err)
	})

	callback := func(e *colly.HTMLElement) {
		title := e.ChildText("div[class*='title-panel'] h6")

		priceStr := e.ChildText("span[class*='sale-price']")
		priceStr = strings.ReplaceAll(priceStr, " ", "")
		priceStr = strings.ReplaceAll(priceStr, "$", "")
		priceStr = strings.ReplaceAll(priceStr, ".", "")
		priceStr = strings.ReplaceAll(priceStr, ",", ".")
		price, err := strconv.ParseFloat(priceStr, 32)
		if err != nil {
			log.Printf("[%s] %s\n", Name, err)
		}

		link := e.ChildAttr("a", "href")
		link = fmt.Sprintf("https://%s%s", domain, link)

		chanResult <- scraping.Match{
			Source: Name,
			Title:  title,
			Price:  math.Round(price*100) / 100,
			Link:   link,
		}
	}

	c.OnHTML(querySelector, callback)

	search = url2.PathEscape(search)
	url := fmt.Sprintf(urlSearch, search)

	err := c.Visit(url)
	if err != nil {
		log.Printf("[%s] %s\n", Name, err)
	}
}
