package scraping

import "fmt"

type Match struct {
	Source string
	Title  string
	Price  float64
	Link   string
}

func (m Match) ToString() string {
	return fmt.Sprintf("Source: %s Title: %s Price: $%.2f Link: %s", m.Source, m.Title, m.Price, m.Link)
}
